/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week1_1;

import java.util.Scanner;

/**
 * Class BmiCalculator
 * @author dcdekker
 */
public class BmiCalculator {

    private final Scanner keyboard = new Scanner(System.in);
    /**
     * The string messages to accompany BMI categories.
     */
    public static final String[] MESSAGES = new String[]{
        "Ondergewicht",
        "Gezond gewicht",
        "Overgewicht",
        "Obesitas",
        "Morbide Obesitas"
    };

    /**
     * starts the application.
     *
     * @param args String with arguments
     */
    public static void main(final String[] args) {
        //implement top-level logic such as fetching user input and creating a feedback message to user
        BmiCalculator bc = new BmiCalculator();
        double userHeigth = bc.getUserHeight();
        double userWeight = bc.getUserWeight();
        double userBMI = bc.calculateBMI(userWeight, userHeigth);
        String label = bc.getMessage(userBMI);

        //generate output to user
        if (label.matches("Gezond gewicht")) {
            System.out.println("Je hebt een " + label);
        } else {
            System.out.println("Je hebt " + label);
        }

    }

    /**
     * fetches the height of the user.
     *
     * @return height the height in meters
     */
    public double getUserHeight() {
        System.out.print("Please give your height, in meters (e.g. 1.84): ");
        String input = keyboard.nextLine();
        double height = 0;
        try {
            height = Double.parseDouble(input);
        } catch (NumberFormatException ex) {
            System.out.println("This is no number! aborting...");
            System.exit(0);
        }
        return height;
    }

    /**
     * fetches the weight of the user.
     *
     * @return weight the weight in kilograms
     */
    public double getUserWeight() {
        System.out.println("Please give your weight, in kilograms");
        String input = keyboard.nextLine();
        double weight = 0;
        try {
            weight = Double.parseDouble(input);
        } catch (NumberFormatException ex) {
            System.out.println("Not a number!");
            System.exit(0);
        }
        return weight;
    }

    /**
     * Returns the BMI given a weight in kilograms and a height in meters.
     *
     * @param weight the weight in kilograms
     * @param height the length in kilograms
     * @return bmi the body mass index
     * @throws IllegalArgumentException when illegal (zero or below) values are
     * provided for weight and/or length
     */
    public double calculateBMI(final double weight, final double height) {
        if (weight <= 0 || height <= 0) {
            throw new IllegalArgumentException("Error: both weight and height should be above 0. Given: weight="
                    + weight + ", height=" + height);
        }
        //Gewicht in kilogram / (Lengte in meter * Lengte in meter)
        double bmi = weight / (height * height);
        return bmi;
    }

    /**
     * Will return an appropriate String representation belonging to a given
     * BMI.
     *
     * BMI index Categorie &lt;18.5 Ondergewicht 18.5 - 25.0 Gezond gewicht 25.0
     * - 30.0 Overgewicht 30.0 - 40.0 Obesitas &gt;40 Morbide Obesitas
     *
     * @param bmi the BMI
     * @return message
     * @throws IllegalArgumentException when illegal (zero or below) BMI value
     * is provided
     */
    public String getMessage(final double bmi) {
        String message = " ";
        if (bmi < 0) {
            throw new IllegalArgumentException("Your bmi cannot be below zero: " + Double.toString(bmi));
        } else if (bmi < 18.5) {
            message = MESSAGES[0];
        } else if (bmi > 18.5 && bmi < 25.0) {
            message = MESSAGES[1];
        } else if (bmi > 25.0 && bmi < 30.0) {
            message = MESSAGES[2];
        } else if (bmi > 30.0 && bmi < 40.0) {
            message = MESSAGES[3];
        } else {
            message = MESSAGES[4];
        }
        return message;
    }
}

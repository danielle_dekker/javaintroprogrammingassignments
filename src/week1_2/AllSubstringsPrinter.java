/*
 * Copyright (c) 2014 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package week1_2;

/**
 * Class AllSubstringsPrinter
 * @author dcdekker
 */
public class AllSubstringsPrinter {

    /**
     * main method serves development purposes only.
     *
     * @param args the args
     */
    public static void main(final String[] args) {
        AllSubstringsPrinter asp = new AllSubstringsPrinter();
        asp.printAllSubstrings("AGCGCT", true, true); //should print left truncated, left aligned
    }

    /**
     * will print all possible substrings according to arguments.
     *
     * @param stringToSubstring the string to substring
     * @param leftTruncated flag to indicate whether the substrings should be
     * truncated from the left (or the right)
     * @param leftAligned flag to indicate whether the substrings should be
     * printed left-aligned (or right-aligned)
     */
    public void printAllSubstrings(
            final String stringToSubstring,
            final boolean leftTruncated,
            final boolean leftAligned) {
        if (leftTruncated == true && leftAligned == true) {
            for (int i = stringToSubstring.length(); i > 0; i--) {
                System.out.println(stringToSubstring.substring(stringToSubstring.length() - i,
                        stringToSubstring.length()));
            }
        } else if (leftTruncated == false && leftAligned == true) {
            for (int i = stringToSubstring.length(); i > 0; i--) {
                System.out.println(stringToSubstring.substring(0, i));
            }
        } else if (leftTruncated == true && leftAligned == false) {
            String space = "";
            for (int i = stringToSubstring.length(); i > 0; i--) {
                System.out.println(space + stringToSubstring.substring(0, i));
                space = " " + space;
            }
        } else {
            String space = "";
            for (int i = 0; i < stringToSubstring.length(); i++) {
                System.out.println(space + stringToSubstring.substring(i, stringToSubstring.length()));
                space = " " + space;
            }
        }

    }

}



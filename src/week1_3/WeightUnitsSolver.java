/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week1_3;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class WeightUnitsSolver {

    /**
     * main to be used for testing purposes
     *
     * @param args
     */
    public static void main(String[] args) {
        WeightUnitsSolver wus = new WeightUnitsSolver();
        wus.convertFromGrams(1000);
    }

    /**
     * will return the number of Pounds, Ounces and Grams represented by this quantity of grams, encapsulated in a
     * BritishWeightUnits object.
     *
     * @param grams integer with amount of grams
     * @return a BritishWeightUnits instance
     * @throws IllegalArgumentException when the Grams quantity is negative
     */
    public BritishWeightUnits convertFromGrams(int grams) {
        if (grams < 0) {
            throw new IllegalArgumentException("Number cannot be negative");
        } else {
            //convert to pounds
            int pounds = (int) Math.round(grams / 454);
            //substract from total
            int leftoverGrams = grams % 454;
            //convert to ounces
            int ounces = (int) Math.round(leftoverGrams / 28);
            //substract from leftover
            int finalLeftoverGrams = leftoverGrams % 28;

            BritishWeightUnits units = new BritishWeightUnits(pounds, ounces, finalLeftoverGrams);
            System.out.println(units);
            return units;
        }
    }
}

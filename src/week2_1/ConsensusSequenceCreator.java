/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week2_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class ConsensusSequenceCreator {

    /**
     * testing main.
     *
     * @param args
     */
    public static void main(String[] args) {
        String[] sequences = new String[4];
        sequences[0] = "GAAT";
        sequences[1] = "GAAA";
        sequences[2] = "GATT";
        sequences[3] = "GAAC";

        ConsensusSequenceCreator csc = new ConsensusSequenceCreator();
        String consensus = csc.createConsensus(sequences, true);
        System.out.println(consensus);
        //consensus should equal "GAWH"
        consensus = csc.createConsensus(sequences, false);
        //consensus should equal "GA[A/T][A/T/C]"
        System.out.println(consensus);
    }

    /**
     * Creates a consensus for a given set of sequences.
     * If iupac flag is true, iupac-type consensus will be given
     * Otherwise a bracket notation will be used
     * @param sequences String Array with sequences
     * @param iupac boolean iupac flag
     * @return a String with created consensus
     */
    public String createConsensus(String[] sequences, boolean iupac) {
        String consensus = "";
        // loop through letters
        for (int character = 0; character < sequences[0].length(); character++) {
            String preConsensus = "";
            // loop through array sequences
            for (int i = 0; i < sequences.length; i++) {
                preConsensus = preConsensus
                        + sequences[i].substring(character, character + 1);
            }
            consensus = consensus + CompareCharacters(preConsensus, iupac);
        }
        return consensus;
    }

    /**
     * Compares characters from a string and returns a consensus.
     * @param characters String
     * @param iupac boolean
     * @return String with reached consensus.
     */
    public String CompareCharacters(String characters, boolean iupac) {

        int stringLength = characters.length();
        // check concurrent nucleotides
        if (characters.matches("[A]{" + stringLength + "}")
                | characters.matches("[C]{" + stringLength + "}")
                | characters.matches("[T]{" + stringLength + "}")
                | (characters.matches("[G]{" + stringLength + "}"))) {
            return characters.substring(0, 1);
            // check dual occurence
        } else if (characters.matches("[AT]+")) {
            if (iupac) {
                return ("W");
            } else {
                return ("[A/T]");
            }
        } else if (characters.matches("[CG]+")) {
            if (iupac) {
                return ("S");
            } else {
                return ("[C/G]");
            }
        } else if (characters.matches("[AC]+")) {
            if (iupac) {
                return ("M");
            } else {
                return ("[A/C]");
            }
        } else if (characters.matches("[GT]+")) {
            if (iupac) {
                return ("K");
            } else {
                return ("[G/T]");
            }
        } else if (characters.matches("[AG]+")) {
            if (iupac) {
                return ("R");
            } else {
                return ("[A/G]");
            }
        } else if (characters.matches("[CT]+")) {
            if (iupac) {
                return ("Y");
            } else {
                return ("[C/T]");
            }
         //Check triple occurences
        } else if (characters.contains("C")
                && characters.contains("G")
                && characters.contains("T")) {
            if (iupac) {
                return ("B");
            } else {
                return ("[C/G/T]");
            }
        } else if (characters.contains("A")
                && characters.contains("T")
                && characters.contains("G")) {
            if (iupac) {
                return ("D");
            } else {
                return ("[A/G/T]");
            }
        } else if (characters.contains("A")
                && characters.contains("C")
                && characters.contains("T")) {
            if (iupac) {
                return ("H");
            } else {
                return ("[A/C/T]");
            }
        } else if (characters.contains("A")
                && characters.contains("C")
                && characters.contains("G")) {
            if (iupac) {
                return ("V");
            } else {
                return ("[A/C/G]");
            }
        } else {
            if (iupac) {
                return ("N");
            } else {
                return ("[A/C/G/T]");
            }
        }
    }

}

/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Animal {
    private int age;
    private String name;
    private double maxSpeed; 
    private double maxAge;
    private String movingType;
       
    /**
     * returns the name of the animal
     * @return name the species name
     */
    public String getName() {
        return this.name;
    }
    /**
     * returns the maximum speed of the animal
     * @return maxSpeed maximum speed
     */
    public double getMaxSpeed() {
        return maxSpeed;
    }
    /**
     * returns the maximum age of animal.
     * @return maxAge maximum age
     */
    public double getMaxAge() {
        return maxAge;
    }
    /**
     * Sets the maximum age of animal.
     * @param maxAge maximum age
     */
    public void setMaxAge(int maxAge){
        this.maxAge = maxAge;
    }

    /**
     * returns the age of animal.
     * @return age 
     */
    public int getAge() {
        return age;
    }
    /**
     * sets the age of animal.
     * @param age 
     */
    public void setAge(int age) {
        this.age = age;
    }
    
    /**
     * returns the movement type
     * @return movementType the way the animal moves
     */
    public String getMovementType() {
        return this.movingType;
    }
    
    /**
     * returns the speed of this animal
     * @return speed the speed of this animal
     */
    public double getSpeed() {
       double speed = maxSpeed * (0.5 + (0.5 * ((maxAge - getAge()) / maxAge)));
       DecimalFormat df = new DecimalFormat("###.#");
       DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(Locale.US);
       df.setDecimalFormatSymbols(dfs);
       speed = Double.parseDouble(df.format(speed));
      // System.out.println(speed);

        return speed;
    }
    
}

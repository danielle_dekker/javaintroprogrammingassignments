/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week3_1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Class AnimalSimulator Prints the name, age, moving type and speed of given animals as arguments.
 *
 * @author Danielle Dekker
 */
public class AnimalSimulator {

    public static void main(String[] args) {
        AnimalSimulator anSim = new AnimalSimulator();
        //String[] testArgs = {"elephant", "21", "horse","2"}; 
        //anSim.start(testArgs);

        anSim.start(args);
    }

    /**
     * Prints the name, age, moving type and speed of an animal. First checks if enough arguments are given ( age for
     * each animal name) Checks if an integer was used as age Loops through animal names given, check if age is valid
     * for this animal If all checks are passed, name, age, moving type and speed will be printed
     *
     * @param args animal names and ages
     */
    private void start(String[] args) {

        //check if help was requested
        if (args.length == 0) {
            printHelp();
        } else if (args[0].matches("help")) {
            printHelp();
        }

        // check for equal length
        if (args.length % 2 == 0) {
            // loop through animal names
            for (int i = 0; i < args.length; i += 2) {
                // check if integer is used for age
                if (checkInteger(i, args)) {
                    int age = Integer.parseInt(args[i + 1]);
                    // loop through animal names
                    if (args[i].toLowerCase().matches("elephant")) {
                        Elephant elephant = createElephant(age);
                        if (age > 0 && age < elephant.getMaxAge()) {
                            System.out.println(elephant.toString());
                        } else {
                            printAgeError(elephant.getName(), elephant.getMaxAge());
                        }
                    } else if (args[i].toLowerCase().matches("horse")) {
                        Horse horse = createHorse(age);
                        if (age > 0 && age < horse.getMaxAge()) {
                            System.out.println(horse.toString());
                        } else {
                            printAgeError(horse.getName(), horse.getMaxAge());
                        }
                    } else if (args[i].toLowerCase().matches("mouse")) {
                        HouseMouse mouse = createMouse(age);
                        if (age > 0 && age < mouse.getMaxAge()) {
                            System.out.println(mouse.toString());
                        } else {
                            printAgeError(mouse.getName(), mouse.getMaxAge());
                        }
                    } else if (args[i].toLowerCase().matches("tortoise")) {
                        Tortoise tortoise = new Tortoise();
                        if (age > 0 && age < tortoise.getMaxAge()) {
                            System.out.println(tortoise.toString());
                        } else {
                            printAgeError(tortoise.getName(), tortoise.getMaxAge());
                        }

                    } else {
                        System.out.print("Error: animal species " + args[i] + " is not known. ");
                        printError();
                    }
                }

            }
        }
    }

    /**
     * Prints an error message for animal name and maximum age.
     *
     * @param animal String
     * @param maxAge double
     */
    public void printAgeError(String animal, double maxAge) {
        System.out.println("Error: maximum age of "
                + animal + " is " + (int) maxAge
                + " years. Please give new values");
    }

    /**
     * Creates an elephant object with given age.
     *
     * @param age integer
     * @return Object Elephant
     */
    public Elephant createElephant(int age) {
        Elephant elephant = new Elephant();
        elephant.setAge(age);
        return elephant;
    }

    /**
     * Creates a horse object with given age.
     *
     * @param age integer
     * @return Object Horse
     */
    public Horse createHorse(int age) {
        Horse horse = new Horse();
        horse.setAge(age);
        return horse;
    }

    /**
     * Creates mouse object with given age.
     *
     * @param age integer
     * @return Object Mouse
     */
    public HouseMouse createMouse(int age) {
        HouseMouse mouse = new HouseMouse();
        mouse.setAge(age);
        return mouse;
    }

    /**
     * Creates tortoise object with given age.
     *
     * @param age integer
     * @return Object Tortoise
     */
    public Tortoise createTortoise(int age) {
        Tortoise tortoise = new Tortoise();
        tortoise.setAge(age);
        return tortoise;
    }

    /**
     * Checks if the given ages are integers.
     *
     * @param i position in String array of animals and ages
     * @param animals String array of animals and ages
     * @return boolean. True if number is array
     */
    public boolean checkInteger(int i, String[] animals) {
        try {
            Integer.parseInt(animals[i + 1]);
            return true;
        } catch (NumberFormatException e) {
            printError();
            return false;
        }
    }

    /**
     * Prints part of standard error message.
     */
    public void printError() {
        System.out.println("run with single parameter \"help\" to get a listing of available species. "
                + "Please give new values");
    }

    /**
     * Prints the help option.
     */
    public void printHelp() {
        List<String> supportedAnimals = getSupportedAnimals();
        System.out.println("Usage: java AnimalSimulator <Species age Species age ...>");
        System.out.println("Supported species (in alphabetical order):");
        for (int i = 0; i < supportedAnimals.size(); i++) {
            System.out.println(i + 1 + ": " + supportedAnimals.get(i));
        }
    }

    /**
     * returns all supported animals as List, alphabetically ordered.
     *
     * @return supportedAnimals the supported animals
     */
    public List<String> getSupportedAnimals() {
        List<String> supportedAnimals = new ArrayList<String>();
        supportedAnimals.add("Elephant");
        supportedAnimals.add("Horse");
        supportedAnimals.add("Mouse");
        supportedAnimals.add("Tortoise");

        return supportedAnimals;
    }
}

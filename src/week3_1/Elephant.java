/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * 
 * @author Danielle Dekker 
 */
public class Elephant extends Animal {
    private String name = "Elephant";
    private double maxSpeed = 40; 
    private double maxAge = 86;
    private String movingType = "thunder";

    @Override
    public double getMaxAge(){
        return maxAge;
    }
    public double getMaxSpeed(){
        return maxSpeed;
    }
    public String getName(){
        return name;
    }
    
    @Override
    public double getSpeed() {
       double speed = maxSpeed * (0.5 + (0.5 * ((maxAge - getAge()) / maxAge)));
       DecimalFormat df = new DecimalFormat("###.#");
       DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(Locale.US);
       df.setDecimalFormatSymbols(dfs);
       speed = Double.parseDouble(df.format(speed));
      // System.out.println(speed);

        return speed;
    }
    @Override
    public String toString(){
        return "An " + name + " of age " + getAge() + " moving in " + movingType + " at " + getSpeed() + " km/h" ;
        
    }
    
    
    
    
    
}

/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week3_1;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Horse extends Animal {

    private String name = "Horse";
    private double maxSpeed = 88;
    private double maxAge = 62;
    private String movingType = "gallop";
    
    @Override
    public double getMaxAge(){
        return maxAge;
    }
    public double getMaxSpeed(){
        return maxSpeed;
    }
    @Override
    public String getName(){
        return name;
    }
    @Override
    public double getSpeed() {
        double speed = maxSpeed * (0.5 + (0.5 * ((maxAge - getAge()) / maxAge)));
        DecimalFormat df = new DecimalFormat("###.#");
       DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(Locale.US);
       df.setDecimalFormatSymbols(dfs);
       speed = Double.parseDouble(df.format(speed));
       return speed;
    }

    @Override
    public String toString() {
        return "A " + name + " of age " + getAge() + " moving in " + movingType + " at " + getSpeed() + " km/h";

    }
}

/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week4_1;

import java.util.Comparator;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Protein implements Comparable<Protein> {

    private final String name;
    private final String accession;
    private final String aminoAcidSequence;
    private GOannotation goAnnotation;
    private final double proteinWeight;

    /**
     * constructs without GO annotation;
     *
     * @param name
     * @param accession
     * @param aminoAcidSequence
     */
    public Protein(String name, String accession, String aminoAcidSequence) {
        this.name = name;
        this.accession = accession;
        if (checkProtein(aminoAcidSequence)) {
            this.aminoAcidSequence = removeDotAndStar(aminoAcidSequence).toUpperCase();
        } else {
            throw new IllegalArgumentException("The entered amino acid sequence: "
                    + aminoAcidSequence + " is not a valid sequence");
        }
        this.proteinWeight = calculateProteinWeight(aminoAcidSequence);

    }

    /**
     * constructs with main properties.
     *
     * @param name
     * @param accession
     * @param aminoAcidSequence
     * @param goAnnotation
     */
    public Protein(String name, String accession, String aminoAcidSequence, GOannotation goAnnotation) {
        this.name = name;
        this.accession = accession;
        this.goAnnotation = goAnnotation;
        if (checkProtein(aminoAcidSequence)) {
            this.aminoAcidSequence = removeDotAndStar(aminoAcidSequence).toUpperCase();
        } else {
            throw new IllegalArgumentException("The entered amino acid sequence: "
                    + aminoAcidSequence + " is not a valid sequence");
        }
        this.proteinWeight = calculateProteinWeight(aminoAcidSequence);
    }

    /**
     * Check if the given sequence is valid.
     *
     * @param sequence string with amino acids sequence
     * @return true if the sequence is valid
     */
    public final boolean checkProtein(String sequence) {
        if (sequence.matches("[ACDEFGHIKLMNPQRSTVWY]*")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Removes any found * or .
     *
     * @param sequence string with amino acids.
     * @return sequence without star and/or dot characters.
     */
    public final String removeDotAndStar(String sequence) {
        sequence = sequence.replace(".", "");
        sequence = sequence.replace("*", "");
        return sequence;
    }

    /**
     * Returns protein Weight
     *
     * @return protein Weight Double
     */
    public Double getProteinWeight() {
        return calculateProteinWeight(aminoAcidSequence);
    }

    @Override
    public int compareTo(Protein other) {
        return name.compareTo(other.getName());
    }

    /**
     * provides a range of possible sorters, based on the type that is requested.
     *
     * @param type
     * @return proteinSorter
     */
    public static Comparator<Protein> getSorter(SortingType type) {
        if (type == SortingType.ACCESSION_NUMBER) {
            return new Comparator<Protein>() {
                @Override
                public int compare(Protein protein, Protein otherProtein) {
                    return protein.getAccession().toLowerCase()
                            .compareTo(otherProtein.getAccession().toLowerCase());
                }
            };
        } else if (type == SortingType.PROTEIN_WEIGHT) {
            return new Comparator<Protein>() {
                @Override
                public int compare(Protein protein, Protein otherProtein) {
                    return Double.compare(otherProtein.getProteinWeight(), protein.getProteinWeight());
                }
            };
        } else if (type == SortingType.PROTEIN_NAME) {
            return new Comparator<Protein>() {
                @Override
                public int compare(Protein protein, Protein otherProtein) {
                    return protein.getName().toLowerCase()
                            .compareTo(otherProtein.getName().toLowerCase());
                }

            };
        } else if (type == SortingType.GO_ANNOTATION) {
            return new Comparator<Protein>() {
                @Override
                public int compare(final Protein protein, final Protein otherProtein) {
                    //first on biological process (3), then on cellular component (1) and last on molecular function (2)
                    int x = protein.getGoAnnotation().getBiologicalProcess().toLowerCase()
                            .compareTo(otherProtein.getGoAnnotation().getBiologicalProcess().toLowerCase());
                    if (x == 0) {
                        x = protein.getGoAnnotation().getCellularComponent().toLowerCase()
                                .compareTo(otherProtein.getGoAnnotation().getCellularComponent().toLowerCase());
                    }
                    if (x == 0) {
                        x = protein.getGoAnnotation().getMolecularFunction().toLowerCase()
                                .compareTo(otherProtein.getGoAnnotation().getMolecularFunction().toLowerCase());
                    }
                    return x;
                }

            };
        } else {
            throw new IllegalArgumentException("This is not a valid sorting type");
        }
    }

    /**
     *
     * @return name the name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return accession the accession number
     */
    public String getAccession() {
        return accession;
    }

    /**
     *
     * @return aminoAcidSequence the amino acid sequence
     */
    public String getAminoAcidSequence() {
        return aminoAcidSequence;
    }

    /**
     *
     * @return GO annotation
     */
    public GOannotation getGoAnnotation() {
        return goAnnotation;
    }

    @Override
    public String toString() {
        return "Biological process : " + goAnnotation.getBiologicalProcess() + " cellular component: " 
                + goAnnotation.getCellularComponent()
                + " molecular function: " + goAnnotation.getMolecularFunction();
        //first on biological process, then on cellular component and last on molecular function
    }
    /**
     * Calculates the weight of a protein.
     *
     * @param aminoAcids String with amino acids
     * @return proteinWeight weight of the protein
     */
    public final double calculateProteinWeight(String aminoAcids) {
        double proteinWeight = 0;
        for (int i = 0; i < aminoAcids.length(); i++) {
            char aminoAcid = aminoAcids.toUpperCase().charAt(i);
            if (aminoAcid == 'A') {
                proteinWeight += 89.0935;
            } else if (aminoAcid == 'C') {
                proteinWeight += 121.1590;
            } else if (aminoAcid == 'D') {
                proteinWeight += 133.1032;
            } else if (aminoAcid == 'E') {
                proteinWeight += 147.1299;
            } else if (aminoAcid == 'G') {
                proteinWeight += 75.0669;
            } else if (aminoAcid == 'H') {
                proteinWeight += 155.1552;
            } else if (aminoAcid == 'I') {
                proteinWeight += 131.1736;
            } else if (aminoAcid == 'K') {
                proteinWeight += 146.1882;
            } else if (aminoAcid == 'L') {
                proteinWeight += 131.1736;
            } else if (aminoAcid == 'M') {
                proteinWeight += 149.2124;
            } else if (aminoAcid == 'N') {
                proteinWeight += 132.1184;
            } else if (aminoAcid == 'P') {
                proteinWeight += 115.1310;
            } else if (aminoAcid == 'Q') {
                proteinWeight += 146.1451;
            } else if (aminoAcid == 'R') {
                proteinWeight += 174.2017;
            } else if (aminoAcid == 'S') {
                proteinWeight += 105.0930;
            } else if (aminoAcid == 'T') {
                proteinWeight += 119.1197;
            } else if (aminoAcid == 'V') {
                proteinWeight += 117.1469;
            } else if (aminoAcid == 'W') {
                proteinWeight += 204.2262;
            } else if (aminoAcid == 'Y') {
                proteinWeight += 181.1894;
            } else if (aminoAcid == 'F') {
                proteinWeight += 165.1900;
            }
        }
        return proteinWeight;
    }

}

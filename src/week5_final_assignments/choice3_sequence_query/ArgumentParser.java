/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5_final_assignments.choice3_sequence_query;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Class ArgumentParser
 * parses given arguments, prints help if necessary
 * @author Danielle Dekker 
 */
public class ArgumentParser {

    private String[] args = null;
    private final Options options;

    /**
     * Constructor. Creates an ArgumentParser with arguments. Adds all options to the CLI parser.
     *
     * @param args String[] with arguments
     */
    public ArgumentParser(final String[] args) {
        this.options = new Options();
        this.args = args;
        options.addOption("help", "help function");
        options.addOption("input", true, "input");
        options.addOption("summary", "give summary");
        options.addOption("to_csv", true, "convert to csv");
        options.addOption("find_prosite", true, "find prosite");
        options.addOption("find_regex", true, "find regex");
        options.addOption("find_ID", true, "find ID");
        options.addOption("find_organism", true, "find organism");
        options.addOption("find_description", true, "find description");
    }

    /**
     * Parses the commandline options. if help is requested, help is printed.
     *
     * @return Object Options[] with options and values
     */
    public Option[] parse() {
        CommandLineParser parser = new DefaultParser();

        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
            Option opts[] = cmd.getOptions();
            // check if help function is requested, or not enough arguments are given
            if (cmd.hasOption("help") || opts.length <= 1) {
                printHelp();
                System.exit(0);
            } else {
                return opts;
            }
        } catch (ParseException e) {
            System.out.println("not a valid option, please try --help");
            System.exit(0);
        }
        return null;
    }

    /**
     * prints the help function.
     */
    private void printHelp() {
        System.out.println("The following functions are available:");
        System.out.println("-input <INFILE> Provide a fasta file to edit");
        System.out.println("-summary Reports a summary of the input file");
        System.out.println("-to_csv <delimiter> Reports a csv overview of the input file");
        System.out.println("-fetch_prosite <PROSITE PATTERN>"
                + "Will report all sequences that contain the Prosite pattern, "
                + "with the location and actual sequence found");
        System.out.println("-find_regex <REGEX_PATTERN> Will report all "
                + "sequences that contain the Regular expression pattern, with"
                + " the location and actual sequence found");
        System.out.println("-find_organism <(PART OF) ORGANISM_NAME> "
                + "Will report all sequences having this wildcard string "
                + "(a regex pattern) in the organism name");
        System.out.println("-find_description <WILDCARD-STRING> Will report"
                + " all sequences having this wildcard string (a regex pattern)"
                + " in the description / sequence name");
        System.out.println("-help Prints this help function");
        System.exit(0);
    }

}

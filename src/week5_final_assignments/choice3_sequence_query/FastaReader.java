/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5_final_assignments.choice3_sequence_query;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Class FastaReader
 * Reads in file with given location by user.
 * @author Danielle Dekker
 */
public class FastaReader {

    private static String fileLocation;

    /**
     * FastaReader constructor.
     *
     * @param fileLocation string with file location
     */
    public FastaReader(final String fileLocation) {
        FastaReader.fileLocation = fileLocation;
    }

   /**
    * Reads in the file.
    * @return Buffered Reader
    */
    public BufferedReader readFile() {
        try {
            FileReader fileReader = new FileReader(fileLocation);
            //Wrap FileReader in BufferedReader.
            BufferedReader bufferedReader
                    = new BufferedReader(fileReader);
            return bufferedReader;

        } catch (FileNotFoundException | NullPointerException e) {
            showErrorMessage();
        }
        return null;
    }
    /**
     * Prints an error message if something is wrong with the infile.
     */
    private void showErrorMessage() {
        System.out.println("Your file: " + fileLocation + " could not be read"
                + " or found. Please try anothor file or try -help for more information.");
        System.exit(0);
    }

    /**
     * Close the file.
     * @param bufferedReader buffered Reader
     */
    public void closeFile(final BufferedReader bufferedReader) {
        try {
            bufferedReader.close();
        } catch (Exception e) {
            e.getMessage();
        }
    }

}

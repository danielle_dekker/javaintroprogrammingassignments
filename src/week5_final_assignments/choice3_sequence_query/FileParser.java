/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5_final_assignments.choice3_sequence_query;

import week5_final_assignments.choice3_sequence_query.fasta_editors.FastaSummary;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import org.apache.commons.cli.Option;
import week5_final_assignments.choice3_sequence_query.fasta_editors.FastaFilter;

/**
 *
 * @author Danielle Dekker
 */
public class FileParser {

    private final Option options[];
    private final BufferedReader reader;
    private final String fileLocation;
    private final HashMap<String, String> fastaEntries;

    public FileParser(Option options[], BufferedReader reader, String fileLocation) {
        this.fastaEntries = new HashMap();
        this.options = options;
        this.reader = reader;
        this.fileLocation = fileLocation;
    }

    /**
     * Reads in the file, and checks which options(s) is/are requested and creates appropriate objects. Prints the
     * result
     */
    public void parse() {
        // read in the file
        readFile();

        // Check which function is requested
        // For each option:
        // Creates required function
        // print the result
        for (Option option : options) {
            switch (option.getOpt()) {
                case "summary":
                    FastaSummary summary = new FastaSummary(fileLocation);
                    summary.createSummary(fastaEntries);
                    System.out.println(summary.getSummary());
                    break;
                case "to_csv":
                    FastaSummary csvSummary = new FastaSummary(fileLocation);
                    csvSummary.createCSVSummary(fastaEntries, option.getValue());
                    System.out.println(csvSummary.getCsvSummary());
                    break;
                case "find_prosite":
                    FastaSummary prositeSummary = new FastaSummary(fileLocation);
                    prositeSummary.createSummaryForRequestedRegions(fastaEntries, option.getValue());
                    System.out.println(prositeSummary.getRequestedRegionSummary());
                    break;
                case "find_regex":
                    FastaSummary regexSummary = new FastaSummary(fileLocation);
                    regexSummary.createSummaryForRequestedRegions(fastaEntries, option.getValue());
                    System.out.println(regexSummary.getRequestedRegionSummary());
                    break;
                case "find_organism":
                    FastaFilter organismFilter = new FastaFilter(fastaEntries);
                    organismFilter.createOrganismFilter(option.getValue());
                    System.out.println(organismFilter.getFilteredOrganism());
                    break;
                case "find_ID":
                    FastaFilter idFilter = new FastaFilter(fastaEntries);
                    idFilter.createIdFilter(option.getValue());
                    System.out.println(idFilter.getFilteredID());
                    break;
                case "find_description":
                    FastaFilter descriptionFilter = new FastaFilter(fastaEntries);
                    descriptionFilter.createDescriptionFilter(option.getValue());
                    System.out.println(descriptionFilter.getFilteredDescription());
                    break;
            }
        }
    }

    /**
     * Reads in a fasta file and puts ID as key with sequence as value into a hashmap.
     *
     * @return HashMap with fasta entries
     */
    private HashMap readFile() {
        String line;
        String iD = "";
        String sequence = "";
        try {
            while ((line = reader.readLine()) != null) {
                if (line.matches(">(.)*")) {
                    iD = line;
                    sequence = "";
                } else {
                    sequence += line;
                    fastaEntries.put(iD, sequence);
                }
            }
            // close file
            reader.close();
        } catch (IOException | NullPointerException e) {
            System.out.println(e.getMessage());
            System.out.println("Cannot read line from file ");
            System.exit(0);
        }
        return fastaEntries;
    }
}

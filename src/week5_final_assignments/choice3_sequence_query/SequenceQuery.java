/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week5_final_assignments.choice3_sequence_query;

import java.io.BufferedReader;
import org.apache.commons.cli.Option;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public final class SequenceQuery {

    /**
     * Main function, executes all.
     *
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        SequenceQuery mainObject = new SequenceQuery();
        /**
         * -help and giving no arguments works
         * -to_csv works; requires separator
         * -summary works
         * -find_prosite works; requires prosite string
         * -find_regex works; requires regex string
         * -find_organism works; requires substring
         * -find_ID works; requires substring
         * -find_description works; requires full string
         * using options in combination works
         */
        mainObject.start(args);

    }

    /**
     * private constructor.
     */
    private SequenceQuery() {
    }

    /**
     * Starts the application. Performs: arguments parsing, gets file location, reads file and parses file.
     *
     * @param arguments String with arguments from user
     */
    private void start(final String[] arguments) {
        // Get argument options and values
        Option options[] = parseArguments(arguments);
        // Get file location
        String fileLocation = getFile(options);
        // Get buffered reader, read in the file
        BufferedReader reader = readFile(fileLocation);
        // Parse file with options
        parseFile(options, reader, fileLocation);

    }

    /**
     * Creates a FileParser to parse file and parses file.
     *
     * @param options List of Options
     * @param reader BufferedReader
     * @param fileLocation String with file location
     */
    public void parseFile(final Option options[], final BufferedReader reader, final String fileLocation) {
        FileParser parser = new FileParser(options, reader, fileLocation);
        parser.parse();

    }

    /**
     * Parse the commandline options.
     *
     * @param arguments commandline arguments
     * @return Object Option[] with options and values.
     */
    public Option[] parseArguments(final String[] arguments) {
        ArgumentParser parser = new ArgumentParser(arguments);
        Option options[] = parser.parse();
        return options;
    }

    /**
     * Reads in the given file.
     *
     * @param inFile String with file location/name
     * @return Object BufferedReader
     */
    public BufferedReader readFile(final String inFile) {
        FastaReader fastaReader = new FastaReader(inFile);
        BufferedReader bufferedReader = fastaReader.readFile();
        //fastaReader.closeFile(bufferedReader);
        return bufferedReader;
    }

    /**
     * Gets the filelocation/name if given.
     *
     * @param options Option object with options and values
     * @return String file location/name
     */
    public String getFile(final Option options[]) {
        for (Option option : options) {
            if (option.getOpt().equals("input")) {
                return option.getValue();
            } else {
                throw new IllegalArgumentException("Not a valid infile");
            }
        }
        return null;
    }
}

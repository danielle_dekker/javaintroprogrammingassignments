/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5_final_assignments.choice3_sequence_query.fasta_editors;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class FastaFilter.
 * @author Danielle Dekker
 */
public class FastaFilter {

    private final HashMap<String, String> fastaEntries;
    private String filteredDescription = "";
    private final String filteredRegex;
    private String filteredOrganism = "";
    private String filteredID = "";

    /**
     * Constructor for a Fasta filter.
     *
     * @param fastaEntries hashmap with fasta entries
     */
    public FastaFilter(final HashMap<String, String> fastaEntries) {
        this.filteredRegex = "";
        this.fastaEntries = fastaEntries;
    }

    /**
     * Looks for matching descriptions in fasta ID.
     * Adds found descriptions to string for output
     * @param description String description as-is
     */
    public void createDescriptionFilter(final String description) {
        //loop through entries
        fastaEntries.keySet().stream().forEach((entry) -> {
            String iD = entry;
            String sequence = fastaEntries.get(entry);
            String sequenceDescription = getSequenceName(iD);
            //find matching description
            if (sequenceDescription.matches(description)) {
                this.filteredDescription += iD + "\n" + sequence + "\n";
            }
        });
    }

    /**
     * Looks for given organism in the ID.
     * If found, adds this to string for output
     * @param organism String organism
     */
    public void createOrganismFilter(final String organism) {
        //loop through entries
        for (String entry : fastaEntries.keySet()) {
            String iD = entry;
            String sequence = fastaEntries.get(entry);
            String sequenceOrganism = getOrganism(iD);
            //search sequence for organism
            if (sequenceOrganism.contains(organism)) {
                this.filteredOrganism += iD + "\n" + sequence + "\n";
            }
        }
    }

    /**
     * Looks for given ID in fasta ID.
     * If Id is found, adds this to output
     * @param iD String with ID
     */
    public void createIdFilter(final String iD) {
        //loop through entries
        fastaEntries.keySet().stream().forEach((entry) -> {
            String sequenceID = entry;
            String sequence = fastaEntries.get(entry);
            //check if ID is present
            if (sequenceID.contains(iD)) {
                this.filteredID += sequenceID + "\n" + sequence + "\n";
            }
        });

    }

    /**
     * Gets the filtered fasta descriptions.
     *
     * @return String with filtered descriptions
     */
    public String getFilteredDescription() {
        return filteredDescription;
    }

    /**
     * Gets the filtered organisms.
     *
     * @return String with filtered fasta sequences by organism.
     */
    public String getFilteredOrganism() {
        return filteredOrganism;
    }

    /**
     * Gets the fasta sequences filtered by ID.
     *
     * @return String with fasta sequences filtered by ID
     */
    public String getFilteredID() {
        return filteredID;
    }

    /**
     * Finds the accession in a fasta ID.
     *
     * @param line String with fasta ID
     * @return String with accession number
     */
    private String getSequenceName(final String line) {
        Pattern pattern = Pattern.compile("\\| (.*)\\[");
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            // remove the | at the start  and [ at the end
            return (matcher.group(0).substring(2, matcher.group(0).length() - 2));
        } else {
            return null;
        }
    }

    /**
     * Finds the organism name from a fasta ID.
     *
     * @param line with fasta ID
     * @return String with organism.
     */
    private String getOrganism(final String line) {
        Pattern pattern = Pattern.compile("\\[(.)*\\]");
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            // remove the [ at the start and ] at the end
            return (matcher.group(0).substring(1, matcher.group(0).length() - 1));
        } else {
            return null;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5_final_assignments.choice3_sequence_query.fasta_editors;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class FastaSummary Facilitates use of different summaries for a fasta file; regular summary, csv summary and summary
 * for prosite.
 *
 * @author Danielle Dekker
 */
public class FastaSummary {

    private String fileName;
    private String sequenceType;
    private String summary;
    private String csvSummary;
    private String requestedRegionSummary;

    /**
     * Creates a fasta summary object.
     *
     * @param fileLocation String with file location
     */
    public FastaSummary(final String fileLocation) {
        this.fileName = fileLocation;
    }

    /**
     * Creates a summary of found prosites or regex.
     * @param requestedRegion String with region to look for
     * @param fastaEntries HashMap with entries from fasta file.
     */
    public void createSummaryForRequestedRegions(final HashMap<String, String> fastaEntries,
            final String requestedRegion) {
        //add prefix
        this.requestedRegionSummary = "ACCNO;NAME;ORGANISM;TYPE;POSITION;SEQ\n";
        String separator = ";";
        //loop through entries
        fastaEntries.keySet().stream().forEach((entry) -> {
            String id = entry;
            String sequence = fastaEntries.get(entry);
            //convert prosite to regex
            String regex = convertPrositeToRegex(requestedRegion);
            //find match
            String regexMatch = getRegexMatch(regex, sequence);
            if (regexMatch.length() > 1) {
                //get accession
                String accession = getAccession(id);
                //get name
                String sequenceName = getSequenceName(id);
                //get organism
                String organism = getOrganism(id);
                // get sequence type
                checkSequenceType(sequence);
                //get match position
                int matchPosition = getSubStringPosition(sequence, regexMatch) + 1;
                //add to the summary
                this.requestedRegionSummary += accession + separator + sequenceName
                        + separator + organism + separator + this.sequenceType
                        + separator + matchPosition + separator + regexMatch
                        + "\n";
            }
        });
    }

    /**
     * creates a summary for fasta file in csv format.
     * @param separator String with requested separator
     * @param fastaEntries HashMap with entries from fasta file
     */
    public void createCSVSummary(final HashMap<String, String> fastaEntries, final String separator) {
        // add prefix
        this.csvSummary = "ACCNO" + separator + "NAME" + separator + "ORGANISM"
                + separator + "TYPE" + separator + "LENGHT" + separator
                + "MOL_WEIGHT\n";
        // loop through entries
        fastaEntries.keySet().stream().forEach((entry) -> {
            String id = entry;
            String sequence = fastaEntries.get(entry);
            //get accession
            String accession = getAccession(id);
            //get name
            String sequenceName = getSequenceName(id);
            //get organism
            String organism = getOrganism(id);
            //get sequence length
            int sequenceLength = sequence.length();
            // get sequence type
            checkSequenceType(sequence);
            //get molecular weight
            double molWeight = getMolecularWeight(sequence);

            this.csvSummary += accession + separator + sequenceName + separator
                    + organism + separator + sequenceLength + separator
                    + this.sequenceType + separator + molWeight + "\n";
        });
    }

    /**
     * Creates a summary for given fasta file.
     *
     * @param fastaEntries Hashmap with entries from fasta
     */
    public void createSummary(final HashMap<String, String> fastaEntries) {
        String sequence = "";
        for (String entry : fastaEntries.keySet()) {
            String iD = entry;
            sequence = fastaEntries.get(entry);
        }
        //get sequence type
        checkSequenceType(sequence);
        //get number of sequences
        int numberOfEntries = countNumberOfEntries(fastaEntries);
        //get average sequence length
        int averageSequenceLength = getAverageLength(fastaEntries);

        this.summary = "file \t\t\t\t" + this.fileName + "\nsequence types\t\t\t"
                + this.sequenceType + "\nnumber of sequences \t\t" + numberOfEntries
                + "\naverage sequence length \t" + averageSequenceLength;
    }

    /**
     * Getter for summary.
     *
     * @return String with summary
     */
    public String getSummary() {
        return this.summary;
    }

    /**
     * Getter for csvSummary.
     *
     * @return String csvSummary
     */
    public String getCsvSummary() {
        return this.csvSummary;
    }

    /**
     * Getter for requested region summary.
     *
     * @return summary of found sequences with region
     */
    public String getRequestedRegionSummary() {
        return this.requestedRegionSummary;
    }

    /**
     * Checks the type of string given.
     * sets sequence type
     * @param sequence String with either a protein or nucleotide sequence
     */
    private void checkSequenceType(final String sequence) {
        if (sequence.matches("[ACTGN]*")) {
            this.sequenceType = "NUCLEOTIDE";
        } else {
            this.sequenceType = "PROTEIN";
        }
    }

    /**
     * counts the number of entries in a hashmap.
     *
     * @param entries hashmap with entries
     * @return int size of the hashmap
     */
    private int countNumberOfEntries(final HashMap<String, String> entries) {
        return entries.size();
    }

    /**
     * calculates the average length of values in a hashmap.
     *
     * @param entries hashMap with entries of which value is string
     * @return average length of the values in a hashmap
     */
    private int getAverageLength(final HashMap<String, String> entries) {
        int totalLength = 0;
        // loop through entries
        for (String entry : entries.keySet()) {
            int sequenceLength = entries.get(entry).length();
            totalLength += sequenceLength;
        }
        return totalLength / entries.size();
    }

    /**
     * Gets the accession from a fasta ID.
     *
     * @param line String with fasta ID
     * @return String with the accession found in the ID
     */
    private String getAccession(final String line) {
        Pattern pattern = Pattern.compile("gi\\|\\d*");
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            return matcher.group(0);
        } else {
            return null;
        }

    }

    /**
     * Gets the name from a fasta ID.
     *
     * @param line String with fasta ID
     * @return String with name found in the ID
     */
    private String getSequenceName(final String line) {
        Pattern pattern = Pattern.compile("\\| (.*)\\[");
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            // remove the | at the start  and [ at the end
            return (matcher.group(0).substring(2, matcher.group(0).length() - 2));
        } else {
            return null;
        }
    }

    /**
     * Finds the organism name from a fasta ID.
     *
     * @param line with fasta ID
     * @return String with organism.
     */
    private String getOrganism(final String line) {
        Pattern pattern = Pattern.compile("\\[(.)*\\]");
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            // remove the [ at the start and ] at the end
            return (matcher.group(0).substring(1, matcher.group(0).length() - 1));
        } else {
            return null;
        }
    }

    /**
     * Calculates either nucleotide or protein mol weight.
     *
     * @param sequence string with sequence
     * @return Double molecular weight
     */
    private double getMolecularWeight(final String sequence) {
        if (sequenceType.equals("PROTEIN")) {
            return calculateProteinWeight(sequence);
        } else {
            return calculateNucleotideWeight(sequence);
        }
    }

    /**
     * Calculate molecular weight of a nucleotide sequence. A= 313.21 T= 304.2 C= 289.18 G=329.21
     *
     * @param nucleotides String with nucleotide sequence
     * @return double with weight
     */
    private double calculateNucleotideWeight(final String nucleotides) {
        int molecularWeight = 0;
        for (int i = 0; i < nucleotides.length(); i++) {
            char nuc = nucleotides.toUpperCase().charAt(i);
            if (nuc == 'A') {
                molecularWeight += 313.21;
            } else if (nuc == 'C') {
                molecularWeight += 289.18;
            } else if (nuc == 'G') {
                molecularWeight += 329.21;
            } else if (nuc == 'T') {
                molecularWeight += 304.12;
            }

        }
        return molecularWeight;
    }

    /**
     * Calculates the weight of a protein.
     *
     * @param aminoAcids String with amino acids
     * @return proteinWeight weight of the protein
     */
    private double calculateProteinWeight(final String aminoAcids) {
        int molecularWeight = 0;
        for (int i = 0; i < aminoAcids.length(); i++) {
            char aminoAcid = aminoAcids.toUpperCase().charAt(i);
            if (aminoAcid == 'A') {
                molecularWeight += 89.0935;
            } else if (aminoAcid == 'C') {
                molecularWeight += 121.1590;
            } else if (aminoAcid == 'D') {
                molecularWeight += 133.1032;
            } else if (aminoAcid == 'E') {
                molecularWeight += 147.1299;
            } else if (aminoAcid == 'G') {
                molecularWeight += 75.0669;
            } else if (aminoAcid == 'H') {
                molecularWeight += 155.1552;
            } else if (aminoAcid == 'I') {
                molecularWeight += 131.1736;
            } else if (aminoAcid == 'K') {
                molecularWeight += 146.1882;
            } else if (aminoAcid == 'L') {
                molecularWeight += 131.1736;
            } else if (aminoAcid == 'M') {
                molecularWeight += 149.2124;
            } else if (aminoAcid == 'N') {
                molecularWeight += 132.1184;
            } else if (aminoAcid == 'P') {
                molecularWeight += 115.1310;
            } else if (aminoAcid == 'Q') {
                molecularWeight += 146.1451;
            } else if (aminoAcid == 'R') {
                molecularWeight += 174.2017;
            } else if (aminoAcid == 'S') {
                molecularWeight += 105.0930;
            } else if (aminoAcid == 'T') {
                molecularWeight += 119.1197;
            } else if (aminoAcid == 'V') {
                molecularWeight += 117.1469;
            } else if (aminoAcid == 'W') {
                molecularWeight += 204.2262;
            } else if (aminoAcid == 'Y') {
                molecularWeight += 181.1894;
            } else if (aminoAcid == 'F') {
                molecularWeight += 165.1900;
            }
        }
        return molecularWeight;
    }

    /**
     * Converts given string to regex.
     *
     * @param prositeInput String with prosite input
     * @return String with regex.
     */
    private String convertPrositeToRegex(final String prositeInput) {
        String regex;
        //convert first part to regex
        regex = prositeInput.replaceAll("-", "");
        //add last part to regex (H[VI]H[VI]H)
        regex = regex.replaceAll("x", ".");
        return regex;
    }

    /**
     * Returns the match, if a match was found.
     * @param regex String with regex to look for
     * @param sequence String to look through with regex
     * @return String with match
     */
    private String getRegexMatch(final String regex, final String sequence) {
        String regexMatch = "";
        // find regex
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(sequence);
        if (matcher.find()) {
            regexMatch = matcher.group(0);
        }
        return regexMatch;
    }

    /**
     * Gets the position of a substring inside a string.     *
     * @param mainString String
     * @param subString String with substring of mainString
     * @return int start position at which the substring is found
     */
    private int getSubStringPosition(final String mainString, final String subString) {
        return mainString.indexOf(subString);
    }

}
